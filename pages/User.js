import React from "react";
import Head from "next/head";
import Nav from "../components/nav";
import { singleUser } from "../utils/api";

class User extends React.Component {
  constructor(props) {
    super();
    this.state = {
      user: "",
      id: props.url.query.id
    };
  }
  async componentDidMount() {
    const id = this.props.url.query.id;
    const response = await singleUser(id);
    this.setState({ user: response });
  }
  userInfo = () => {
    const { user } = this.state;
    if (user) {
      return (
        <ul>
          <li>{user.name}</li>
          <li>{user.email}</li>
          <li>{user.phone}</li>
        </ul>
      );
    }
  };

  render() {
    return (
      <div>
        <Head>
          <title>User - {this.state.id}</title>
          <meta description="Single User `" />
        </Head>
        <Nav />
        {this.userInfo()}
      </div>
    );
  }
}
export default User;
