import React from "react";
import Head from "next/head";
import Nav from "../components/nav";
import App from "next/app";
import Link from "next/link";
import { fetchUsers } from "../utils/api";
class Home extends React.Component {
  state = {
    name: "vishal",
    users: []
  };
  componentDidMount() {
    this.fetchUsers();
  }
  fetchUsers = async () => {
    const response = await fetchUsers();
    this.setState({ users: response });
  };

  render() {
    const { users } = this.state;
    return (
      <div>
        <Head>
          <title>Home</title>
          <meta description="All Users" />
        </Head>
        <Nav />
        <h2>User List</h2>
        <ul>
          {users.map(user => {
            return (
              <Link href={`/user?id=${user.id}`} key={user.id}>
                <li key={user.id}>
                  {user.name} {user.email}
                </li>
              </Link>
            );
          })}
        </ul>
      </div>
    );
  }
}

export default Home;
