export const fetchUsers = async () => {
  const response = await fetch("https://jsonplaceholder.typicode.com/users");
  if (response.ok) {
    let json = await response.json();
    return json;
  }
};

export const singleUser = async id => {
  const response = await fetch(
    `https://jsonplaceholder.typicode.com/users/${id}`
  );
  if (response.ok) {
    let json = await response.json();
    return json;
  }
};
